<?php 
require_once("menu.php");

function menuShow($arr, $css, $sort) 
{
	$arr = menuSort($arr, $sort);
	include("menu_Show.php");
	
}
function menuSort($arr, $sort, $key = 'sort')
{	
	usort($arr, function($a, $b) use ($key, $sort) {
		return $sort == SORT_ASC ? ($a[$key] <=> $b[$key]) : ($b[$key] <=> $a[$key]);
	});
	return $arr;
}
function isCurUrl($url) 
{
    return $url == parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
}
function showMenuItem($menu) 
{
    $currPage ='';

    foreach ($menu as $item) {
        
        if (isCurUrl($item['path'])) {
            $currPage = $item['title'];
            break;
        }
    }

    return $currPage; 
}
function strTrim($string, $length = 12) 
{	
	if (strlen($string) > $length) {
		$string = substr($string,0, 12) . "...";
	}
	return $string;
}