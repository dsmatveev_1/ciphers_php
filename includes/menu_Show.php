<?php
$itemsFinal = "";

foreach ($arr as $key => $value) {
	$itemClass = isCurUrl($value["path"]) ?  'class="decoration-underlined"' : 'class="decoration-none"';
	/*$itemTitle = strTrim($value["title"]);*/
	$itemsFinal .= '<li><a href="' . $value["path"] . '" ' . $itemClass . '>' . $value["title"] . '</a></li>';
}

?>

<div class="clearfix">
    <ul class="<?=$css?>">
        <?=$itemsFinal ?>
    </ul>
</div>