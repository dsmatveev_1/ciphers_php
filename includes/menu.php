<?php 
$menu = [
	[
		"title" => "Шифр простой замены",
		"path" => "/route/easyChange/",
		"sort" => 2,
	],
		[
		"title" => "Aффинный шифр",
		"path" => "/route/athenian/",
		"sort" => 3,
	],
	[
		"title" => "Реккурентный аффинный шифр",
		"path" => "/route/athenianReq/",
		"sort" => 4,
	],
	[
		"title" => "Главная",
		"path" => "/",
		"sort" => 1,
	],
    [
        "title" => "Шифр Хилла",
        "path" => "/route/hill",
        "sort" => 5,
    ],
    [
        "title" => "Рекуррентный шифр Хилла",
        "path" => "/route/hillReq",
        "sort" => 6,
    ],
];