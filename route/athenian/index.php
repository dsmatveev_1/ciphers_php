<?php 
include_once($_SERVER['DOCUMENT_ROOT'] . "/templates/header.php");
include("athenianFunctions.php");
?>



<table width="100%" border="0" cellspacing="0" cellpadding="0">
    	<tr>
        	<td class="left-collum-index">
				<h1><?=showMenuItem($menu)?></h1>
				<p>Другим частным случаем шифра простой замены является аффинный шифр, основанный на так называемом аффинном преобразовании.</p>
				<p>Данный шифр реализует замену символов открытого текста с использованием операций в кольце классов вычетов. Символы алфавита A мощностью m представляются элементами кольца классов вычетов Zm.</p>
				<p>В качестве ключа аффинного шифра выступает пара значений k = (α, β), α ∈ Zm∗ , β ∈ Ζm, соответственно ключевое пространство имеет вид K = Zm∗ × Zm.</p>
				<p>Открытый текст и шифртекст обозначим соответственно x = (x1, ... , xl) и y = (y1, ... , yl), где xi ∈ Zm, yi ∈ Zm, i = 1, l.</p>
				<p>Зашифрование отдельного символа открытого текста осуществляется по формуле yi = αxi + β, i = 1, l, расшифрование — по формуле xi = (yi − β)α−1, i = 1, l.</p>
			</td>
            <td class="right-collum-index">
				</div>
				<div class="index-auth">
                    <form action="/route/athenian/" method="post">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="iat">
                                    <label for="original_text">Исходный текст:</label>
                                    <input id="original_text" size="30" name="textEncrypt" value="<?=isset($_POST["textEncrypt"]) ? $_POST["textEncrypt"] : "" ?>">
                                </td>
							</tr>
							<tr>
								<td class="iat">
                                    <label for="key">Ключ 1:</label>
                                    <input id="key" size="30" name="key1Encrypt" value="<?=isset($_POST["textEncrypt"]) ? $_POST["key1Encrypt"] : "" ?>">
                                </td>
							</tr>
							<tr>
								<td class="iat">
                                    <label for="key">Ключ 2:</label>
                                    <input id="key" size="30" name="key2Encrypt" value="<?=isset($_POST["textEncrypt"]) ? $_POST["key2Encrypt"] : "" ?>">
                                </td>
							</tr>
							<tr>
								<td class="iat">
									<select name="alphabetEncrypt">
									  <option name="ru" value="ru">Русский алфавит</option>
									  <option name="eng">Английский алфавит</option>
									</select>
                                </td>
							</tr>
							<tr>
								<td><input type="submit" value="Зашифровать"></td>
							</tr>

							<?php  if (isset($_POST["textEncrypt"]) && $_POST["textEncrypt"]!="" && $_POST["key1Encrypt"]!=""): ?>
							<tr><td class="out">Зашифрованный текст: <?php encrypt($_POST["textEncrypt"], $_POST["key1Encrypt"], $_POST["key2Encrypt"], $_POST["alphabetEncrypt"]); endif;?> </td> </tr>

						</table>
                    </form>
                    
				</div>
				<div class="index-auth">
                    <form action="/route/athenian/" method="post">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="iat">
                                    <label for="original_text">Шифртекст:</label>
                                    <input id="original_text" size="30" name="textDecrypt" value="<?=isset($_POST["textDecrypt"]) ? $_POST["textDecrypt"] : "" ?>">
                                </td>
							</tr>
							<tr>
								<td class="iat">
                                    <label for="key">Ключ 1:</label>
                                    <input id="key" size="30" name="key1Decrypt" value="<?=isset($_POST["textDecrypt"]) ? $_POST["key1Decrypt"] : "" ?>">
                                </td>
							</tr>
							<tr>
								<td class="iat">
                                    <label for="key">Ключ 2:</label>
                                    <input id="key" size="30" name="key2Decrypt" value="<?=isset($_POST["textDecrypt"]) ? $_POST["key2Decrypt"] : "" ?>">
                                </td>
							</tr>
							<tr>
								<td class="iat">
									<select name="alphabetDecrypt">
									  <option name="ru" value="ru">Русский алфавит</option>
									  <option name="eng">Английский алфавит</option>
									</select>
                                </td>
							</tr>
							<tr>
								<td><input type="submit" value="Расшифровать"></td>
							</tr>
							<?php  if (isset($_POST["textDecrypt"]) && $_POST["textDecrypt"]!="" && $_POST["key1Decrypt"]!=""): ?>
							<tr><td class="out">Расшифрованный текст: <?php Decrypt($_POST["textDecrypt"], $_POST["key1Decrypt"], $_POST["key2Decrypt"], $_POST["alphabetDecrypt"]); endif;?> </td> </tr>
						</table>
                    </form>
				</div>
			</td> 
        </tr>
    </table>








<?php               
require_once($_SERVER['DOCUMENT_ROOT'] . '/templates/footer.php');
?>