<?php

function encrypt($text, $keyWord1, $keyWord2, $alph)
{
	//require_once($_SERVER['DOCUMENT_ROOT'] . "/ciphers_php/includes/alphabets.php");
    $engH = array(
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
        's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '.', ',', ' '
    );
    $ruH = array(
        'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р',
        'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я', '.', ',', ' ', '?'
    );

    $alpCur = ($alph == "ruH") ? $ruH : $engH;
	$text = mb_strtolower($text);
    $keyWord1 = mb_strtolower($keyWord1);
    $keyWord2 = mb_strtolower($keyWord2);

    //Создается массив с порядковыми числами букв слова-ключа1
    $keyWordArr1 = mb_str_split($keyWord1);
	$keyWordAsNums1 = [];

    foreach ($keyWordArr1 as $key => $value) {
        for ($i = 0; $i < count($alpCur); $i++) {
            if ($value == $alpCur[$i]) {
                $keyWordAsNums1[] = $i;
            };
        }
    }

    //Создается массив с порядковыми числами букв слова-ключа2
    $keyWordArr2 = mb_str_split($keyWord2);
    $keyWordAsNums2 = [];

    foreach ($keyWordArr2 as $key => $value) {
        for ($i = 0; $i < count($alpCur); $i++) {
            if ($value == $alpCur[$i]) {
                $keyWordAsNums2[] = $i;
            };
        }
    }

    //Создается матрица слова-ключа1
	$keyAsMatrix1 = [];
    $sqrt = sqrt(mb_strlen($keyWord1));
    $keyLen = mb_strlen($keyWord1);
    $i = 0;
    if ($sqrt*$sqrt == $keyLen) {
        foreach ($keyWordAsNums1 as $key => $value){
            if ($key % $sqrt == 0) {
                $i = $key/$sqrt;
            }
            $keyAsMatrix1[$i][] = $value;
        }
    } else return "Длина введенного слова1 не является квадратом числа";

    //Создается матрица слова-ключа2
    $keyAsMatrix2 = [];
    $sqrt = sqrt(mb_strlen($keyWord1));
    $keyLen = mb_strlen($keyWord1);
    $i = 0;
    if ($sqrt*$sqrt == $keyLen) {
        foreach ($keyWordAsNums2 as $key => $value){
            if ($key % $sqrt == 0) {
                $i = $key/$sqrt;
            }
            $keyAsMatrix2[$i][] = $value;
        }
    } else return "Длина введенного слова2 не является квадратом числа";



    //Проверяем длину исходного текста, если она не кратна размерности матрицы ключа, добавляем в конец пробелы
    if (mb_strlen($text) % $sqrt != 0) {
        do {
            $text .= ' ';
        } while (mb_strlen($text) % $sqrt != 0);
    }
    // Создаем массив матриц ключей
    $numOfTextBlocks = mb_strlen($text) / $sqrt;
    $matrixArr = matrixes($keyAsMatrix1, $keyAsMatrix2,$numOfTextBlocks, count($alpCur));

    //Разбиваем исходный текст на блоки размерностью n, где n - размерность матрицы ключа
	$textSizeN = mb_str_split($text, $sqrt);

    //Создается массив порядковыми числами букв исходного текста
    $keyText = [];
    foreach ($textSizeN as $key => $value) {
        $value = mb_str_split($value);
        foreach ($value as $item) {
            for ($i = 0; $i < count($alpCur); $i++) {
                if ($item == $alpCur[$i]) {
                    $keyText[$key][] = $i;
                };
            }
        }
    }
    //Кладем еще в один масссив каждый блок для индивидуальной работы с каждым
    foreach ($keyText as $key => $value) {
        $arr[] = $value;
        $keyText[$key]= $arr;
        $arr = [];
    }


    $finTextInArr = [];
    for ($i = 0; $i < count($keyText); $i++) {
        $finTextInArr[] = matrixmult($keyText[$i], $matrixArr[$i]);
    }

    //По индексам расшифровываем в буквы
	$textFinal = '';
	foreach ($finTextInArr as $value) {
	    foreach ($value as $item) {
            foreach ($item as $key1 => $value2) {
                foreach ($alpCur as $key2 => $item1) {
                    if ($value2 % count($alpCur) == $key2) {
                        $textFinal .= $item1;
                    }
                }
            }
        }
	}
	return $textFinal;
}

function decrypt($text, $keyWord1, $keyWord2, $alph)
{
    //require_once($_SERVER['DOCUMENT_ROOT'] . "/ciphers_php/includes/alphabets.php");
    $engH = array(
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
        's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '.', ',', ' '
    );
    $ruH = array(
        'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р',
        'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я', '.', ',', ' ', '?'
    );


    $alpCur = ($alph == "ruH") ? $ruH : $engH;
    $text = mb_strtolower($text);
    $keyWord1 = mb_strtolower($keyWord1);
    $keyWord2 = mb_strtolower($keyWord2);

    //Создается массив с порядковыми числами букв слова-ключа1
    $keyWordArr1 = mb_str_split($keyWord1);
    $keyWordAsNums1 = [];

    foreach ($keyWordArr1 as $key => $value) {
        for ($i = 0; $i < count($alpCur); $i++) {
            if ($value == $alpCur[$i]) {
                $keyWordAsNums1[] = $i;
            };
        }
    }

    //Создается массив с порядковыми числами букв слова-ключа2
    $keyWordArr2 = mb_str_split($keyWord2);
    $keyWordAsNums2 = [];

    foreach ($keyWordArr2 as $key => $value) {
        for ($i = 0; $i < count($alpCur); $i++) {
            if ($value == $alpCur[$i]) {
                $keyWordAsNums2[] = $i;
            };
        }
    }

    //Создается матрица слова-ключа1
    $keyAsMatrix1 = [];
    $sqrt = sqrt(mb_strlen($keyWord1));
    $keyLen = mb_strlen($keyWord1);
    $i = 0;
    if ($sqrt*$sqrt == $keyLen) {
        foreach ($keyWordAsNums1 as $key => $value){
            if ($key % $sqrt == 0) {
                $i = $key/$sqrt;
            }
            $keyAsMatrix1[$i][] = $value;
        }
    } else return "Длина введенного слова1 не является квадратом числа";

    //Создается матрица слова-ключа2
    $keyAsMatrix2 = [];
    $sqrt = sqrt(mb_strlen($keyWord1));
    $keyLen = mb_strlen($keyWord1);
    $i = 0;
    if ($sqrt*$sqrt == $keyLen) {
        foreach ($keyWordAsNums2 as $key => $value){
            if ($key % $sqrt == 0) {
                $i = $key/$sqrt;
            }
            $keyAsMatrix2[$i][] = $value;
        }
    } else return "Длина введенного слова2 не является квадратом числа";



    //Проверяем длину исходного текста, если она не кратна размерности матрицы ключа, добавляем в конец пробелы
    if (mb_strlen($text) % $sqrt != 0) {
        do {
            $text .= ' ';
        } while (mb_strlen($text) % $sqrt != 0);
    }

    //Разбиваем исходный текст на блоки размерностью n, где n - размерность матрицы ключа
    $textSizeN = mb_str_split($text, $sqrt);

    //Создается массив порядковыми числами букв исходного текста
    $keyText = [];
    foreach ($textSizeN as $key => $value) {
        $value = mb_str_split($value);
        foreach ($value as $item) {
            for ($i = 0; $i < count($alpCur); $i++) {
                if ($item == $alpCur[$i]) {
                    $keyText[$key][] = $i;
                };
            }
        }
    }
    //Кладем еще в один масссив каждый блок для индивидуальной работы с каждым
    foreach ($keyText as $key => $value) {
        $arr[] = $value;
        $keyText[$key]= $arr;
        $arr = [];
    }

    $determinant1 = determinat($keyAsMatrix1, $sqrt-1);
    $determinant2 = determinat($keyAsMatrix2, $sqrt-1);

    //Находим обратный детерминанту1 элемент в кольце по модулю длины алфавита.
    for ($i = 0; $i < count($alpCur); $i++){
        if (($determinant1 * $i % count($alpCur)) == 1){
            $determinantInverse1 = $i;
            break;
        }
    }
    //Находим обратный детерминанту2 элемент в кольце по модулю длины алфавита.
    for ($i = 0; $i < count($alpCur); $i++){
        if (($determinant2 * $i % count($alpCur)) == 1){
            $determinantInverse2 = $i;
            break;
        }
    }
    //Находим матрицу алгебраических дополнений1 и сразу делим по модулю на длину алфавита
    $MatrixOfAlgDop1 = [];
    $minors1 = Deter($keyAsMatrix1, $sqrt-1);
    $k1 = 0;
    for ($i = 0; $i < count($keyAsMatrix1); $i++) {
        for ($j = 0; $j < count($keyAsMatrix1); $j++) {
            $MatrixOfAlgDop1[$i][$j] = pow(-1, $i+$j) * $minors1[$k1];
            $MatrixOfAlgDop1[$i][$j] = $MatrixOfAlgDop1[$i][$j] % count($alpCur); //сразу делим по модулю на длину алфавита
            $MatrixOfAlgDop1[$i][$j] = $MatrixOfAlgDop1[$i][$j] * $determinantInverse1; //Умножаем на обратный детерминанту элемент
            $MatrixOfAlgDop1[$i][$j] = $MatrixOfAlgDop1[$i][$j] % count($alpCur); //делим по модулю на длину алфавита
            $k1++;
        }
    }
    //Находим матрицу алгебраических дополнений1 и сразу делим по модулю на длину алфавита
    $MatrixOfAlgDop2 = [];
    $minors2 = Deter($keyAsMatrix2, $sqrt-1);
    $k2 = 0;
    for ($i = 0; $i < count($keyAsMatrix2); $i++) {
        for ($j = 0; $j < count($keyAsMatrix2); $j++) {
            $MatrixOfAlgDop2[$i][$j] = pow(-1, $i+$j) * $minors2[$k2];
            $MatrixOfAlgDop2[$i][$j] = $MatrixOfAlgDop2[$i][$j] % count($alpCur); //сразу делим по модулю на длину алфавита
            $MatrixOfAlgDop2[$i][$j] = $MatrixOfAlgDop2[$i][$j] * $determinantInverse2; //Умножаем на обратный детерминанту элемент
            $MatrixOfAlgDop2[$i][$j] = $MatrixOfAlgDop2[$i][$j] % count($alpCur); //делим по модулю на длину алфавита
            $k2++;
        }
    }

    //транспонируем полученную матрицу1
    $transMatrix1 = trans($MatrixOfAlgDop1);
    //транспонируем полученную матрицу2
    $transMatrix2 = trans($MatrixOfAlgDop2);

    //Получим матрицу1, обратную ключу. Если элемент матрицы отрицательный, меняем его на другой, вычисленный по формуле count($alpCur)+<элемент>
    for ($i = 0; $i < count($transMatrix1); $i++) {
        for ($j = 0; $j < count($transMatrix1); $j++) {
            if ($transMatrix1[$i][$j] < 0) {
                $transMatrix1[$i][$j] += count($alpCur);
            }
        }
    }
    //Получим матрицу2, обратную ключу. Если элемент матрицы отрицательный, меняем его на другой, вычисленный по формуле count($alpCur)+<элемент>
    for ($i = 0; $i < count($transMatrix2); $i++) {
        for ($j = 0; $j < count($transMatrix2); $j++) {
            if ($transMatrix2[$i][$j] < 0) {
                $transMatrix2[$i][$j] += count($alpCur);
            }

        }
    }

    // Создаем массив матриц ключей
    $numOfTextBlocks = mb_strlen($text) / $sqrt;
    $matrixArr = matrixesDec($transMatrix1, $transMatrix2, $numOfTextBlocks, count($alpCur));



    //Для дешифровки шифротекста умножаем строки шифротекста на матрицу обратную ключу.
    $finTextInArr = [];
    for ($i = 0; $i < count($keyText); $i++) {
        $finTextInArr[] = matrixmult($keyText[$i], $matrixArr[$i]);
    }

    foreach ($finTextInArr as $key => $value) {
        foreach ($value as $key1 => $item) {
            foreach ($item as $key2 => $item1) {
                $finTextInArr[$key][$key1][$key2] = $item1 % count($alpCur);
            }
        }
    }

    //По индексам расшифровываем в буквы
    $textFinal = '';
    foreach ($finTextInArr as $value) {
        foreach ($value as $item) {
            foreach ($item as $key1 => $value2) {
                foreach ($alpCur as $key2 => $item1) {
                    if ($value2 % count($alpCur) == $key2) {
                        $textFinal .= $item1;
                    }
                }
            }
        }
    }
    return $textFinal;
}

//Функция создания массива матриц ключей
function matrixes($matrix1, $matrix2, $numOfTextBlocks, $alphLen) {
    $matrixOfKeys = [];
    $matrixOfKeys[] = $matrix1;
    $matrixOfKeys[] = $matrix2;

    for ($i = 1; $i < $numOfTextBlocks - 1; $i++){
        $temp = matrixmult($matrixOfKeys[$i], $matrixOfKeys[$i-1]);

        foreach ($temp as $key1 => $item) {
            foreach ($item as $key2 => $value) {
                $temp[$key1][$key2] = $value % $alphLen;
            }
        }
        $matrixOfKeys[] = $temp;
    }
    return $matrixOfKeys;
}

function matrixesDec($matrix1, $matrix2, $numOfTextBlocks, $alphLen) {
    $matrixOfKeys = [];
    $matrixOfKeys[] = $matrix1;
    $matrixOfKeys[] = $matrix2;

    for ($i = 1; $i < $numOfTextBlocks - 1; $i++){
        $temp = matrixmult($matrixOfKeys[$i-1], $matrixOfKeys[$i]);

        foreach ($temp as $key1 => $item) {
            foreach ($item as $key2 => $value) {
                $temp[$key1][$key2] = $value % $alphLen;
            }
        }
        $matrixOfKeys[] = $temp;
    }
    return $matrixOfKeys;
}

function matrixmult($m1,$m2){
    $r=count($m1);
    $c=count($m2[0]);
    $p=count($m2);
    //if(count($m1[0])!=$p){throw new Exception('Incompatible matrixes');}
    $m3=array();
    for ($i=0;$i < $r;$i++){
        for($j=0;$j<$c;$j++){
            $m3[$i][$j]=0;
            for($k=0;$k<$p;$k++){
                $m3[$i][$j]+=$m1[$i][$k]*$m2[$k][$j];
            }
        }
    }
    return($m3);
}

/*
*определитель матриц
*/

function minor($m1, $n, $i, $j)
{
    for ($a=0;$a<$n or $a==$n;$a++){
        for ($b=0;$b<$n or $b==$n;$b++){
            if ($a<$i and $b<$j){
                $r[$a][$b]=$m1[$a][$b];
            }

            if ($a<$i and $b>$j){
                $r[$a][$b-1]=$m1[$a][$b];
            }

            if ($a>$i and $b<$j){
                $r[$a-1][$b]=$m1[$a][$b];
            }

            if ($a>$i and $b>$j){
                $r[$a-1][$b-1]=$m1[$a][$b];
            }
        }
    }

    return $r;
}
function Det($m0, $n) {
    $V1 = 0;
    //если минор дошел до размера 1*1
    if ($n==0){
        $V1=$V1+$m0[0][0];
    }
    //если минор дошел до размера 2*2
    if ($n==1){
        $V1=$V1+($m0[0][0]*$m0[1][1])-($m0[0][1]*$m0[1][0]);
    }
    //если минор дошел до размера 3*3 и более
    if ($n>1){
        for ($a = 0; $a < 1; $a++){ //раскладываем по первой строке
            for ($b=0;$b<$n or $b==$n;$b++){

                $mino=minor($m0, $n, $a, $b); //минор
                $c=$n-1;
                //раскладываем по первой строке
                $V1 = $V1 + $m0[0][$b]*pow((-1), 1+$b+1) * Det ($mino,$c); //сумма  значение элемента * -1^($a+$b)* минор$a$b
                //так как ключи цикла масссива начинаются с 0, то для разложения по перво строки нужно писать индексы 1+$b+1
            }
        } }
    return $V1;
}


function determinat ($arrXX, $n){ // от массива и его размерности
    //$V=1;
    switch ($n) {
        case 0: //матрица 1*1
            $V=$arrXX[0][0];
            return $V;
            break;

        case 1: //матрица 2*2
            $V=($arrXX[0][0]*$arrXX[1][1])-($arrXX[0][1]*$arrXX[1][0]);
            return $V;
            break;
        default:
            $V = Det($arrXX, $n);
            return $V;
    }


}
function Deter($m0, $n) {

    //если минор дошел до размера 1*1
    if ($n==0){
        $V1=$V1+$$m0[0][0];
    }
    //если минор дошел до размера 2*2
    if ($n==1){
        $V1=$V1+($m0[0][0]*$m0[1][1])-($m0[0][1]*$m0[1][0]);
    }
    //если минор дошел до размера 3*3 и более
    if ($n>1){
        for ($a = 0; $a < $n+1; $a++){ //раскладываем по первой строке
            for ($b=0;$b<$n or $b==$n;$b++){
                $mino=minor($m0, $n, $a, $b); //минор

                $Dets[] = Det ($mino,$n-1);
            }
        }
    }
    return $Dets;
}

function trans(array $matrix)
{
    $last = sizeof($matrix) - 1;
    eval('$result = array_map(null, $matrix['
        . implode('], $matrix[', range(0, $last)) . ']);');
    return $result;
}

