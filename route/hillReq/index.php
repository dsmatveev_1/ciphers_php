<?php 
include_once($_SERVER['DOCUMENT_ROOT'] . "/templates/header.php");
include("hillReqFunctions.php");
?>



<table width="100%" border="0" cellspacing="0" cellpadding="0">
    	<tr>
        	<td class="left-collum-index">
				<h1><?=showMenuItem($menu)?></h1>

			</td>
            <td class="right-collum-index">
				</div>
				<div class="index-auth">
                    <form action="/route/hillReq/" method="post">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="iat">
                                    <label for="original_text">Исходный текст:</label>
                                    <input id="original_text" size="30" name="textEncrypt" value="<?=isset($_POST["textEncrypt"]) ? $_POST["textEncrypt"] : "" ?>">
                                </td>
							</tr>
							<tr>
								<td class="iat">
                                    <label for="key">Ключ-1 (слово, длина которого квадрат числа):</label>
                                    <input id="key" size="30" name="key1Encrypt" value="<?=isset($_POST["key1Encrypt"]) ? $_POST["key1Encrypt"] : "" ?>">
                                </td>
							</tr>
                            <tr>
                                <td class="iat">
                                    <label for="key">Ключ-2 (слово, длина которого квадрат числа):</label>
                                    <input id="key" size="30" name="key2Encrypt" value="<?=isset($_POST["key2Encrypt"]) ? $_POST["key2Encrypt"] : "" ?>">
                                </td>
                            </tr>
							<tr>
								<td class="iat">
									<select name="alphabetEncrypt">
									    <option name="ruH" value="ruH">Русский алфавит</option>
                                        <option name="engH" value="engH">Английский алфавит</option>
									</select>
                                </td>
							</tr>
							<tr>
								<td><input type="submit" value="Зашифровать"></td>
							</tr>

							<?php  if (isset($_POST["textEncrypt"]) && $_POST["textEncrypt"]!="" && $_POST["key1Encrypt"]!=""): ?>
							<tr><td class="out">Зашифрованный текст: <?php echo encrypt($_POST["textEncrypt"], $_POST["key1Encrypt"],$_POST["key2Encrypt"], $_POST["alphabetEncrypt"]); endif;?> </td> </tr>

						</table>
                    </form>
                    
				</div>
				<div class="index-auth">
                    <form action="/route/hillReq/" method="post">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="iat">
                                    <label for="original_text">Шифртекст:</label>
                                    <input id="original_text" size="30" name="textDecrypt" value="<?=isset($_POST["textDecrypt"]) ? $_POST["textDecrypt"] : "" ?>">
                                </td>
							</tr>
                            <tr>
                                <td class="iat">
                                    <label for="key">Ключ-1 (слово, длина которого квадрат числа):</label>
                                    <input id="key" size="30" name="key1Decrypt" value="<?=isset($_POST["textDecrypt"]) ? $_POST["key1Decrypt"] : "" ?>">
                                </td>
                            </tr>
                            <tr>
                                <td class="iat">
                                    <label for="key">Ключ-2 (слово, длина которого квадрат числа):</label>
                                    <input id="key" size="30" name="key2Decrypt" value="<?=isset($_POST["textDecrypt"]) ? $_POST["key2Decrypt"] : "" ?>">
                                </td>
                            </tr>
								<td class="iat">
                                    <select name="alphabetDecrypt">
									  <option name="ruH" value="ruH">Русский алфавит</option>
									  <option name="engH" value="engH">Английский алфавит</option>
									</select>
                                </td>
							</tr>
							<tr>
								<td><input type="submit" value="Расшифровать"></td>
							</tr>
							<?php  if (isset($_POST["textDecrypt"]) && $_POST["textDecrypt"]!="" && $_POST["key1Decrypt"]!=""): ?>
							<tr><td class="out">Расшифрованный текст: <?php  print_r(Decrypt($_POST["textDecrypt"], $_POST["key1Decrypt"],$_POST["key2Decrypt"], $_POST["alphabetDecrypt"])); endif;?> </td> </tr>
						</table>
                    </form>
				</div>
			</td> 
        </tr>
    </table>








<?php               
require_once($_SERVER['DOCUMENT_ROOT'] . '/templates/footer.php');
?>