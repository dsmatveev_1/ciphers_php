<?php

function encrypt($text, $keyWord, $alph)
{
	//require_once($_SERVER['DOCUMENT_ROOT'] . "/ciphers_php/includes/alphabets.php");
    $engH = array(
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
        's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '.', ',', ' '
    );
    $ruH = array(
        'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р',
        'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я', '.', ',', ' ', '?'
    );

    $alpCur = ($alph == "ruH") ? $ruH : $engH;
	$text = mb_strtolower($text);

    //Создается массив с порядковыми числами букв слова-ключа
    $keyWordArr = mb_str_split($keyWord);
	$keyWordAsNums = [];

    foreach ($keyWordArr as $key => $value) {
        for ($i = 0; $i < count($alpCur); $i++) {
            if ($value == $alpCur[$i]) {
                $keyWordAsNums[] = $i;
            };
        }
    }

    //Создается матрица слова-ключа
	$keyAsMatrix = [];
    $sqrt = sqrt(mb_strlen($keyWord));
    $keyLen = mb_strlen($keyWord);
    $i = 0;
    if ($sqrt*$sqrt == $keyLen) {
        foreach ($keyWordAsNums as $key => $value){
            if ($key % $sqrt == 0) {
                $i = $key/$sqrt;
            }
            $keyAsMatrix[$i][] = $value;
        }
    } else return "Длина введенного слова не является квадратом числа";


    //Проверяем длину исходного текста, если она не кратна размерности матрицы ключа, добавляем в конец пробелы
    if (mb_strlen($text) % $sqrt != 0) {
        do {
            $text .= ' ';
        } while (mb_strlen($text) % $sqrt != 0);
    }

    //Разбиваем исходный текст на блоки размерностью n, где n - размерность матрицы ключа
	$textSizeN = mb_str_split($text, $sqrt);

    //Создается массив порядковыми числами букв исходного текста
    $keyText = [];
    foreach ($textSizeN as $key => $value) {
        $value = mb_str_split($value);
        foreach ($value as $item) {
            for ($i = 0; $i < count($alpCur); $i++) {
                if ($item == $alpCur[$i]) {
                    $keyText[$key][] = $i;
                };
            }
        }
    }

    $finTextInArr = matrixmult($keyText, $keyAsMatrix);

    //По индексам расшифровываем в буквы
	$textFinal = '';
	foreach ($finTextInArr as $value) {
	    foreach ($value as $key1 => $value2)
            foreach ($alpCur as $key2 => $item) {
                if ($value2 % count($alpCur) == $key2) {
                    $textFinal .= $item;
                }
            }
	}
	return $textFinal;
}

function decrypt($text, $keyWord, $alph)
{
    //require_once($_SERVER['DOCUMENT_ROOT'] . "/ciphers_php/includes/alphabets.php");
    $engH = array(
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
        's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '.', ',', ' '
    );
    $ruH = array(
        'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р',
        'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я', '.', ',', ' ', '?'
    );


    $alpCur = ($alph == "ruH") ? $ruH : $engH;
    $text = mb_strtolower($text);

    //Создается массив с порядковыми числами букв слова-ключа
    $keyWordArr = mb_str_split($keyWord);
    $keyWordAsNums = [];
    foreach ($keyWordArr as $key => $value) {
        for ($i = 0; $i < count($alpCur); $i++) {
            if ($value == $alpCur[$i]) {
                $keyWordAsNums[] = $i;
            };
        }
    }


    //Создается матрица слова-ключа
    $keyAsMatrix = [];
    $sqrt = sqrt(mb_strlen($keyWord));
    $keyLen = mb_strlen($keyWord);

    $i = 0;
    if ($sqrt*$sqrt == $keyLen) {
        foreach ($keyWordAsNums as $key => $value){
            if ($key % $sqrt == 0) {
                $i = $key/$sqrt;
            }
            $keyAsMatrix[$i][] = $value;
        }
    } else return "Длина введенного слова не является квадратом числа";

    //Разбиваем исходный текст на блоки размерностью n, где n - размерность матрицы ключа
    $textSizeN = mb_str_split($text, $sqrt);
    //Создается массив c порядковыми числами букв исходного текста
    $keyText = [];
    foreach ($textSizeN as $key => $value) {
        $value = mb_str_split($value);
        foreach ($value as $item) {
            for ($i = 0; $i < count($alpCur); $i++) {
                if ($item == $alpCur[$i]) {
                    $keyText[$key][] = $i;
                };
            }
        }
    }

    $determinant = determinat($keyAsMatrix, $sqrt-1);
    //Находим обратный детерминанту элемент в кольце по модулю длины алфавита.
    for ($i = 0; $i < count($alpCur); $i++){
        if (($determinant * $i % count($alpCur)) == 1){
            $determinantInverse = $i;
            break;
        }
    }
    //Находим матрицу алгебраических дополнений и сразу делим по модулю на длину алфавита
    $MatrixOfAlgDop = [];
    $minors = Deter($keyAsMatrix, $sqrt-1);
    $k = 0;
    for ($i = 0; $i < count($keyAsMatrix); $i++) {
        for ($j = 0; $j < count($keyAsMatrix); $j++) {
            $MatrixOfAlgDop[$i][$j] = pow(-1, $i+$j) * $minors[$k];
            $MatrixOfAlgDop[$i][$j] = $MatrixOfAlgDop[$i][$j] % count($alpCur); //сразу делим по модулю на длину алфавита
            $MatrixOfAlgDop[$i][$j] = $MatrixOfAlgDop[$i][$j] * $determinantInverse; //Умножаем на обратный детерминанту элемент
            $MatrixOfAlgDop[$i][$j] = $MatrixOfAlgDop[$i][$j] % count($alpCur); //делим по модулю на длину алфавита
            $k++;
        }
    }

    //транспонируем полученную матрицу
    $transMatrix = trans($MatrixOfAlgDop);
    //Получим матрицу, обратную ключу. Если элемент матрицы отрицательный, меняем его на другой, вычисленный по формуле count($alpCur)+<элемент>
    for ($i = 0; $i < count($transMatrix); $i++) {
        for ($j = 0; $j < count($transMatrix); $j++) {
            if ($transMatrix[$i][$j] < 0) {
                $transMatrix[$i][$j] += count($alpCur);
            }

        }
    }



    //Для дешифровки шифротекста умножаем строки шифротекста на матрицу обратную ключу.
    $finTextInArr = matrixmult($keyText, $transMatrix);

    foreach ($finTextInArr as $key => $value) {
        foreach ($value as $key1 => $item) {
            $finTextInArr[$key][$key1] = $item % count($alpCur);
        }
    }

    $textFinal = '';
    foreach ($finTextInArr as $key1 => $value) {
        foreach ($value as $key2 => $item) {
            foreach ($alpCur as $key => $value){
                if ($finTextInArr[$key1][$key2] == $key) {
                    $finTextInArr[$key1][$key2] = $value;
                    $textFinal.= $value;
                }
            }
        }
    }


    return $textFinal;
}

function matrixmult($m1,$m2){
    $r=count($m1);
    $c=count($m2[0]);
    $p=count($m2);
    //if(count($m1[0])!=$p){throw new Exception('Incompatible matrixes');}
    $m3=array();
    for ($i=0;$i < $r;$i++){
        for($j=0;$j<$c;$j++){
            $m3[$i][$j]=0;
            for($k=0;$k<$p;$k++){
                $m3[$i][$j]+=$m1[$i][$k]*$m2[$k][$j];
            }
        }
    }
    return($m3);
}

/*
*определитель матриц
*/

function minor($m1, $n, $i, $j)
{
    for ($a=0;$a<$n or $a==$n;$a++){
        for ($b=0;$b<$n or $b==$n;$b++){
            if ($a<$i and $b<$j){
                $r[$a][$b]=$m1[$a][$b];
            }

            if ($a<$i and $b>$j){
                $r[$a][$b-1]=$m1[$a][$b];
            }

            if ($a>$i and $b<$j){
                $r[$a-1][$b]=$m1[$a][$b];
            }

            if ($a>$i and $b>$j){
                $r[$a-1][$b-1]=$m1[$a][$b];
            }
        }
    }

    return $r;
}
function Det($m0, $n) {
    $V1 = 0;
    //если минор дошел до размера 1*1
    if ($n==0){
        $V1=$V1+$m0[0][0];
    }
    //если минор дошел до размера 2*2
    if ($n==1){
        $V1=$V1+($m0[0][0]*$m0[1][1])-($m0[0][1]*$m0[1][0]);
    }
    //если минор дошел до размера 3*3 и более
    if ($n>1){
        for ($a = 0; $a < 1; $a++){ //раскладываем по первой строке
            for ($b=0;$b<$n or $b==$n;$b++){

                $mino=minor($m0, $n, $a, $b); //минор
                $c=$n-1;
                //раскладываем по первой строке
                $V1 = $V1 + $m0[0][$b]*pow((-1), 1+$b+1) * Det ($mino,$c); //сумма  значение элемента * -1^($a+$b)* минор$a$b
                //так как ключи цикла масссива начинаются с 0, то для разложения по перво строки нужно писать индексы 1+$b+1
            }
        } }
    return $V1;
}


function determinat ($arrXX, $n){ // от массива и его размерности
    //$V=1;
    switch ($n) {
        case 0: //матрица 1*1
            $V=$arrXX[0][0];
            return $V;
            break;

        case 1: //матрица 2*2
            $V=($arrXX[0][0]*$arrXX[1][1])-($arrXX[0][1]*$arrXX[1][0]);
            return $V;
            break;
        default:
            $V = Det($arrXX, $n);
            return $V;
    }


}
function Deter($m0, $n) {

    //если минор дошел до размера 1*1
    if ($n==0){
        $V1=$V1+$$m0[0][0];
    }
    //если минор дошел до размера 2*2
    if ($n==1){
        $V1=$V1+($m0[0][0]*$m0[1][1])-($m0[0][1]*$m0[1][0]);
    }
    //если минор дошел до размера 3*3 и более
    if ($n>1){
        for ($a = 0; $a < $n+1; $a++){ //раскладываем по первой строке
            for ($b=0;$b<$n or $b==$n;$b++){
                $mino=minor($m0, $n, $a, $b); //минор

                $Dets[] = Det ($mino,$n-1);
            }
        }
    }
    return $Dets;
}

function trans(array $matrix)
{
    $last = sizeof($matrix) - 1;
    eval('$result = array_map(null, $matrix['
        . implode('], $matrix[', range(0, $last)) . ']);');
    return $result;
}

