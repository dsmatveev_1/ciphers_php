<?php 
include_once($_SERVER['DOCUMENT_ROOT'] . "/templates/header.php");
include("hillFunctions.php");
?>



<table width="100%" border="0" cellspacing="0" cellpadding="0">
    	<tr>
        	<td class="left-collum-index">
				<h1><?=showMenuItem($menu)?></h1>

			</td>
            <td class="right-collum-index">
				</div>
				<div class="index-auth">
                    <form action="/route/hill/" method="post">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="iat">
                                    <label for="original_text">Исходный текст:</label>
                                    <input id="original_text" size="30" name="textEncrypt" value="<?=isset($_POST["textEncrypt"]) ? $_POST["textEncrypt"] : "" ?>">
                                </td>
							</tr>
							<tr>
								<td class="iat">
                                    <label for="key">Ключ (слово, длина которого квадрат числа):</label>
                                    <input id="key" size="30" name="keyEncrypt" value="<?=isset($_POST["keyEncrypt"]) ? $_POST["keyEncrypt"] : "" ?>">
                                </td>
							</tr>
							<tr>
								<td class="iat">
									<select name="alphabetEncrypt">
									    <option name="ruH" value="ruH">Русский алфавит</option>
                                        <option name="engH" value="engH">Английский алфавит</option>
									</select>
                                </td>
							</tr>
							<tr>
								<td><input type="submit" value="Зашифровать"></td>
							</tr>

							<?php  if (isset($_POST["textEncrypt"]) && $_POST["textEncrypt"]!="" && $_POST["keyEncrypt"]!=""): ?>
							<tr><td class="out">Зашифрованный текст: <?php echo encrypt($_POST["textEncrypt"], $_POST["keyEncrypt"], $_POST["alphabetEncrypt"]); endif;?> </td> </tr>

						</table>
                    </form>
                    
				</div>
				<div class="index-auth">
                    <form action="/route/hill/" method="post">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="iat">
                                    <label for="original_text">Шифртекст:</label>
                                    <input id="original_text" size="30" name="textDecrypt" value="<?=isset($_POST["textDecrypt"]) ? $_POST["textDecrypt"] : "" ?>">
                                </td>
							</tr>
                            <tr>
                                <td class="iat">
                                    <label for="key">Ключ (слово, длина которого квадрат числа):</label>
                                    <input id="key" size="30" name="keyDecrypt" value="<?=isset($_POST["textDecrypt"]) ? $_POST["keyDecrypt"] : "" ?>">
                                </td>
                            </tr>
								<td class="iat">
                                    <select name="alphabetDecrypt">
									  <option name="ruH" value="ruH">Русский алфавит</option>
									  <option name="engH" value="engH">Английский алфавит</option>
									</select>
                                </td>
							</tr>
							<tr>
								<td><input type="submit" value="Расшифровать"></td>
							</tr>
							<?php  if (isset($_POST["textDecrypt"]) && $_POST["textDecrypt"]!="" && $_POST["keyDecrypt"]!=""): ?>
							<tr><td class="out">Расшифрованный текст: <?php  print_r(Decrypt($_POST["textDecrypt"], $_POST["keyDecrypt"], $_POST["alphabetDecrypt"])); endif;?> </td> </tr>
						</table>
                    </form>
				</div>
			</td> 
        </tr>
    </table>








<?php               
require_once($_SERVER['DOCUMENT_ROOT'] . '/templates/footer.php');
?>