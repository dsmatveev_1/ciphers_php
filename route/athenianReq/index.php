<?php 
include_once($_SERVER['DOCUMENT_ROOT'] . "/templates/header.php");
include("athenianReqFunctions.php");
?>



<table width="100%" border="0" cellspacing="0" cellpadding="0">
    	<tr>
        	<td class="left-collum-index">
				<h1><?=showMenuItem($menu)?></h1>
				<p>Усилением аффинного шифра является аффинный рекуррентный шифр, когда для каждого символа открытого текста вычисляется новое ключевое значение на основе предыдущего. Для этого необходимо задать две ключевые пары k1 = (α1, β1), k2 = (α2, β2), и тогда ключевая пара для произвольного символа преобразуемой последовательности будет иметь вид ki = (αi−1αi−2, βi−1 + βi−2), i = 3, l.</p>
			</td>
            <td class="right-collum-index">
				</div>
				<div class="index-auth">
                    <form action="/route/athenianReq/" method="post">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="iat">
                                    <label for="original_text">Исходный текст:</label>
                                    <input id="original_text" size="30" name="textEncrypt" value="<?=isset($_POST["textEncrypt"]) ? $_POST["textEncrypt"] : "" ?>">
                                </td>
							</tr>
							<tr>
								<td class="iat">
                                    <label for="key">Ключ-пара 1:</label>
                                    <input id="key" size="30" name="key1Encrypt" value="<?=isset($_POST["textEncrypt"]) ? $_POST["key1Encrypt"] : "" ?>">
                                </td>
							</tr>
							<tr>
								<td class="iat">
                                    <label for="key">Ключ-пара 2:</label>
                                    <input id="key" size="30" name="key2Encrypt" value="<?=isset($_POST["textEncrypt"]) ? $_POST["key2Encrypt"] : "" ?>">
                                </td>
							</tr>
							<tr>
								<td class="iat">
									<select name="alphabetEncrypt">
									  <option name="ru" value="ru">Русский алфавит</option>
									  <option name="eng">Английский алфавит</option>
									</select>
                                </td>
							</tr>
							<tr>
								<td><input type="submit" value="Зашифровать"></td>
							</tr>

							<?php  if (isset($_POST["textEncrypt"]) && $_POST["textEncrypt"]!="" && $_POST["key1Encrypt"]!=""): ?>
							<tr><td class="out">Зашифрованный текст: <?php encrypt($_POST["textEncrypt"], $_POST["key1Encrypt"], $_POST["key2Encrypt"], $_POST["alphabetEncrypt"]); endif;?> </td> </tr>

						</table>
                    </form>
                    
				</div>
				<div class="index-auth">
                    <form action="/route/athenianReq/" method="post">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="iat">
                                    <label for="original_text">Шифртекст:</label>
                                    <input id="original_text" size="30" name="textDecrypt" value="<?=isset($_POST["textDecrypt"]) ? $_POST["textDecrypt"] : "" ?>">
                                </td>
							</tr>
							<tr>
								<td class="iat">
                                    <label for="key">Ключ-пара 1:</label>
                                    <input id="key" size="30" name="key1Decrypt" value="<?=isset($_POST["textDecrypt"]) ? $_POST["key1Decrypt"] : "" ?>">
                                </td>
							</tr>
							<tr>
								<td class="iat">
                                    <label for="key">Ключ-пара 2:</label>
                                    <input id="key" size="30" name="key2Decrypt" value="<?=isset($_POST["textDecrypt"]) ? $_POST["key2Decrypt"] : "" ?>">
                                </td>
							</tr>
							<tr>
								<td class="iat">
									<select name="alphabetDecrypt">
									  <option name="ru" value="ru">Русский алфавит</option>
									  <option name="eng">Английский алфавит</option>
									</select>
                                </td>
							</tr>
							<tr>
								<td><input type="submit" value="Расшифровать"></td>
							</tr>
							<?php  if (isset($_POST["textDecrypt"]) && $_POST["textDecrypt"]!="" && $_POST["key1Decrypt"]!=""): ?>
							<tr><td class="out">Расшифрованный текст: <?php Decrypt($_POST["textDecrypt"], $_POST["key1Decrypt"], $_POST["key2Decrypt"], $_POST["alphabetDecrypt"]); endif;?> </td> </tr>
						</table>
                    </form>
				</div>
			</td> 
        </tr>
    </table>








<?php               
require_once($_SERVER['DOCUMENT_ROOT'] . '/templates/footer.php');
?>