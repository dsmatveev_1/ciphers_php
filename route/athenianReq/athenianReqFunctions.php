<?php

function encrypt($text, $key1, $key2, $alph)
{
	require_once($_SERVER['DOCUMENT_ROOT'] . "/includes/alphabets.php");

	$key1 =  preg_split('//u', str_replace(' ', '', $key1), -1, PREG_SPLIT_NO_EMPTY);
	$key2 =  preg_split('//u', str_replace(' ', '', $key2), -1, PREG_SPLIT_NO_EMPTY);

	$alpCur = ($alph == "ru") ? $ru : $eng;

	$text = preg_split('//u', mb_strtolower(str_replace(' ', '', $text)), -1, PREG_SPLIT_NO_EMPTY);
	
	$keys1 = keys1($key1[0], $key1[1], count($text), count($alpCur));
	$keys2 = keys2($key2[0], $key2[1], count($text), count($alpCur));

	$keyText = [];
	foreach ($text as $key => $value) {
		for ($i = 0; $i < count($alpCur); $i++) {
			if ($value == $alpCur[$i]) {
				$keyText[] = $i;
			};
		}
	}

	$textKey = [];
	foreach ($keyText as $key => $value) {
		$textKey[] = ($value*$keys1[$key] + $keys2[$key]) % count($alpCur);
	}

	$textFinal = [];
	foreach ($textKey as $value) {
		foreach ($alpCur as $key => $item) {
			if ($value == $key) {
				$textFinal[] = $item;
			}
		}
	}

	include("resultShow.php");
}

function decrypt($text, $key1, $key2, $alph)
{
	require_once($_SERVER['DOCUMENT_ROOT'] . "/includes/alphabets.php");

	$key1 =  preg_split('//u', str_replace(' ', '', $key1), -1, PREG_SPLIT_NO_EMPTY);
	$key2 =  preg_split('//u', str_replace(' ', '', $key2), -1, PREG_SPLIT_NO_EMPTY);

	$alpCur = ($alph == "ru") ? $ru : $eng;

	$text = preg_split('//u', mb_strtolower(str_replace(' ', '', $text)), -1, PREG_SPLIT_NO_EMPTY);
	
	
	$keyText = [];
	foreach ($text as $key => $value) {
		for ($i = 0; $i < count($alpCur); $i++) {
			if ($value == $alpCur[$i]) {
				$keyText[] = $i;
			};
		}
	}


	for ($i = 0; $i < count($alpCur); $i++){
		if (($key1[0] * $i % count($alpCur)) == 1){
			$key1[0] = $i;
			break;			
		}
	}
	
		for ($i = 0; $i < count($alpCur); $i++){
		if (($key1[1] * $i % count($alpCur)) == 1){
			$key1[1] = $i;
			break;			
		}
	}

	$keys1 = keys1($key1[0], $key1[1], count($text), count($alpCur));
	$keys2 = keys2($key2[0], $key2[1], count($text), count($alpCur));


	$textKey = [];
	foreach ($keyText as $key => $value) {
		if (($value - $keys2[$key]) * (count($alpCur) - $keys1[$key]) % count($alpCur) < 0) {
			$textKey[] = ($value - $keys2[$key]) * ($keys1[$key]) % count($alpCur) + count($alpCur);
		} else {
			$textKey[] = ($value - $keys2[$key]) * ($keys1[$key]) % count($alpCur);
		}
		
	}
	
	$textFinal = [];
	foreach ($textKey as $value) {
		foreach ($alpCur as $key => $item) {
			if ($value == $key) {
				$textFinal[] = $item;
			}
		}
	}

	include("resultShow.php");
}


function keys1($key1_1, $key1_2, $textLen, $alphLen) 
{
	$keys1 = [];
	$keys1[] = $key1_1;
	$keys1[] = $key1_2;
	
	for ($i = 1; $i < $textLen - 1; $i++){
		$keys1[] = $keys1[$i] * $keys1[$i - 1] % $alphLen;
	}
	return $keys1;
}

function keys2($key2_1, $key2_2, $textLen, $alphLen) 
{
	$keys2 = [];
	$keys2[] = $key2_1;
	$keys2[] = $key2_2;
	
	for ($i = 1; $i < $textLen - 1; $i++){
		$keys2[] = $keys2[$i] + $keys2[$i - 1] % $alphLen;
	}
	return $keys2;
}