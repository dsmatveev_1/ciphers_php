<?php

function encrypt($text, $key, $alph)
{
	require_once($_SERVER['DOCUMENT_ROOT'] . "/includes/alphabets.php");
	
	$key = explode(" ", $key);
	
	$alpCur = ($alph == "ru") ? $ru : $eng;

	$keyAlph = getKeyAlph($key, $alpCur);

	$text = preg_split('//u', mb_strtolower(str_replace(' ', '', $text)), -1, PREG_SPLIT_NO_EMPTY);
	

	$keyText = [];
	foreach ($text as $key => $value) {
		for ($i = 0; $i < count($alpCur); $i++) {
			if ($value == $alpCur[$i]) {
				$keyText[] = $i + 1;
			};
		}
	}

	$textFinal = [];
	foreach ($keyText as $value) {
		foreach ($keyAlph as $key => $item) {
			if ($value == $key + 1) {
				$textFinal[] = $item;
			}
		}
	}
	include("resultShow.php");
}



function Decrypt($text, $key, $alph)
{
	require_once($_SERVER['DOCUMENT_ROOT'] . "/includes/alphabets.php");
	
	$key = explode(" ", $key);
	
	$alpCur = ($alph == "ru") ? $ru : $eng;
	
	$keyAlph = getKeyAlph($key, $alpCur);

	$text = preg_split('//u', mb_strtolower(str_replace(' ', '', $text)), -1, PREG_SPLIT_NO_EMPTY);
	
	$keyText = [];
	foreach ($text as $key => $value) {
		for ($i = 0; $i < count($keyAlph); $i++) {
			if ($value == $keyAlph[$i]) {
				$keyText[] = $i + 1;
			};
		}
	}

	$textFinal = [];
	foreach ($keyText as $value) {
		foreach ($alpCur as $key => $item) {
			if ($value == $key + 1) {
				$textFinal[] = $item;
			}
		}
	}
	include("resultShow.php");
}


function getKeyAlph($key, $alpCur)
{
	$keyAlph = [];
	foreach ($key as $value) {
		foreach ($alpCur as $key => $item) {
			if ($value == $key + 1) {
				$keyAlph[] = $item;
			};
		}
	}
	return $keyAlph;
}