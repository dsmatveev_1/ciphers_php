<?php 
include_once($_SERVER['DOCUMENT_ROOT'] . "/templates/header.php");
include("easyChangeFunctions.php");
?>



<table width="100%" border="0" cellspacing="0" cellpadding="0">
    	<tr>
        	<td class="left-collum-index">
				<h1><?=showMenuItem($menu)?></h1>
				<p>Простейшим примером подстановочного шифра является шифр простой замены.
				<p>Математически данный шифр может быть описан на языке подстановок.</p>
				<p>Каждой букве алфавита A мощностью m ставится в соответствие число из диапазона 1 ... m — другими словами, все символы алфавита нумеруются.</p>
				<p>Множество возможных ключей шифра простой замены является симметрической группой степени m, то есть группой подстановок длины m: K = S(A) = Sm.</p>
				<p>Открытый текст обозначим x = (x1, ... , xl) , где xi ∈ A, i = 1, l, соответствующий шифртекст — y = (y1, ... , yl).</p>
				<p>Зашифрование открытого текста x = (x1, ... , xl) на ключе k ∈ K может быть записано как Ek(x) = (k(x1), ... , k(xl)), расшифрование шифртекста y = (y1, ... , yl) натом же ключе — Dk(y) = (k−1(y1), ... , k−1(yl)), где k−1 ∈ K — подстановка, обратная k.</p>
				<p>Проще говоря, при шифровании каждый символ текста заменяется на другой символ с помощью ключевой подстановки.</p>
				<p>Известным частным случаем шифра простой замены является шифр Цезаря, названный так по имени использовавшего его всю жизнь древнеримского полководца. Данный шифр основан на использовании одного-единственного ключа — подстановки, полученной циклическим сдвигом элементов второй строки относительно первой на три позиции влево.</p>
			</td>
            <td class="right-collum-index">
				</div>
				<div class="index-auth">
                    <form action="/route/easyChange/" method="post">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="iat">
                                    <label for="original_text">Исходный текст:</label>
                                    <input id="original_text" size="30" name="textEncrypt" value="<?=isset($_POST["textEncrypt"]) ? $_POST["textEncrypt"] : "" ?>">
                                </td>
							</tr>
							<tr>
								<td class="iat">
                                    <label for="key">Ключ:</label>
                                    <input id="key" size="30" name="keyEncrypt" value="<?=isset($_POST["textEncrypt"]) ? $_POST["keyEncrypt"] : "" ?>">
                                </td>
							</tr>
							<tr>
								<td class="iat">
									<select name="alphabetEncrypt">
									  <option name="ru" value="ru">Русский алфавит</option>
									  <option name="eng">Английский алфавит</option>
									</select>
                                </td>
							</tr>
							<tr>
								<td><input type="submit" value="Зашифровать"></td>
							</tr>

							<?php  if (isset($_POST["textEncrypt"]) && $_POST["textEncrypt"]!="" && $_POST["keyEncrypt"]!=""): ?>
							<tr><td class="out">Зашифрованный текст: <?php encrypt($_POST["textEncrypt"], $_POST["keyEncrypt"], $_POST["alphabetEncrypt"]); endif;?> </td> </tr>

						</table>
                    </form>
                    
				</div>
				<div class="index-auth">
                    <form action="/route/easyChange/" method="post">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="iat">
                                    <label for="original_text">Шифртекст:</label>
                                    <input id="original_text" size="30" name="textDecrypt" value="<?=isset($_POST["textDecrypt"]) ? $_POST["textDecrypt"] : "" ?>">
                                </td>
							</tr>
							<tr>
								<td class="iat">
                                    <label for="key">Ключ:</label>
                                    <input id="key" size="30" name="keyDecrypt" value="<?=isset($_POST["textDecrypt"]) ? $_POST["keyDecrypt"] : "" ?>">
                                </td>
							</tr>
							<tr>
								<td class="iat">
									<select name="alphabetDecrypt">
									  <option name="ru" value="ru">Русский алфавит</option>
									  <option name="eng">Английский алфавит</option>
									</select>
                                </td>
							</tr>
							<tr>
								<td><input type="submit" value="Расшифровать"></td>
							</tr>
							<?php  if (isset($_POST["textDecrypt"]) && $_POST["textDecrypt"]!="" && $_POST["keyDecrypt"]!=""): ?>
							<tr><td class="out">Расшифрованный текст: <?php Decrypt($_POST["textDecrypt"], $_POST["keyDecrypt"], $_POST["alphabetDecrypt"]); endif;?> </td> </tr>
						</table>
                    </form>
				</div>
			</td> 
        </tr>
    </table>








<?php               
require_once($_SERVER['DOCUMENT_ROOT'] . '/templates/footer.php');
?>